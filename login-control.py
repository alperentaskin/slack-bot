import os
import requests
import glob


test_results_dir = '/Users/Alperen/pycharm-projects/unique-login-robotframework/results'

def post_slack(file_name):
    my_file = {
      'file' : (test_results_dir+'/'+file_name, open(test_results_dir+'/'+file_name, 'rb'), 'png')
    }

    token=""

    payload={
      "filename":test_results_dir+'/'+file_name,
      "token":token,
      "channels":['#general'],
      "initial_comment":"Login yapılamadı."
    }

    requests.post("https://slack.com/api/files.upload", params=payload, files=my_file)

ssCounter = len(glob.glob1(test_results_dir,"*.png"))

if ssCounter > 0:
    screen_shot = glob.glob1(test_results_dir, "*.png")[0]
    post_slack(screen_shot)
    os.remove(test_results_dir+"/"+screen_shot)

